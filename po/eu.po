# Basque translation for icon-tool.
# Copyright (C) 2019 icon-tool's COPYRIGHT HOLDER
# This file is distributed under the same license as the icon-tool package.
# Asier Sarasua Garmendia <asiersarasua@ni.eus>, 2019, 2022, 2023.
#
msgid ""
msgstr "Project-Id-Version: icon-tool master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/design/app-icon-preview/issues\n"
"POT-Creation-Date: 2023-04-03 10:52+0000\n"
"PO-Revision-Date: 2023-04-06 10:00+0100\n"
"Last-Translator: Asier Sarasua Garmendia <asiersarasua@ni.eus>\n"
"Language-Team: Basque <librezale@librezale.eus>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: data/org.gnome.design.AppIconPreview.desktop.in.in:3
#: data/org.gnome.design.AppIconPreview.metainfo.xml.in.in:8 src/main.rs:22
#: src/widgets/project_previewer.rs:91 src/widgets/project_previewer.rs:200
msgid "App Icon Preview"
msgstr "Aplikazio-ikonoen Aurrebista"

#: data/org.gnome.design.AppIconPreview.desktop.in.in:4
msgid "Preview applications icons"
msgstr "Aplikazio-ikonoen aurrebista"

#: data/org.gnome.design.AppIconPreview.gschema.xml.in:6
msgid "The latest opened files separated by a comma"
msgstr "Irekitako azken fitxategiak, komaz bananduta"

#: data/org.gnome.design.AppIconPreview.metainfo.xml.in.in:9
msgid "Tool for designing applications icons"
msgstr "Aplikazio-ikonoak diseinatzeko tresna"

#: data/org.gnome.design.AppIconPreview.metainfo.xml.in.in:11
msgid ""
"App Icon Preview is a tool for designing icons which target the GNOME "
"desktop."
msgstr "Aplikazio-ikonoen Aurrebista aplikazioa GNOME mahaigainean erabili daitezkeen ikonoak diseinatzeko tresna da."

#: data/org.gnome.design.AppIconPreview.metainfo.xml.in.in:22
msgid "Previewing an Application icon"
msgstr "Aplikazio-ikono baten aurrebista"

#: data/org.gnome.design.AppIconPreview.metainfo.xml.in.in:109
msgid "Bilal Elmoussaoui &amp; Zander Brown"
msgstr "Bilal Elmoussaoui &amp; Zander Brown"

#: data/resources/ui/export.ui:16
msgid ""
"Export the icon for production use. Off-canvas objects are removed and the "
"SVG is optimised for size"
msgstr "Esportatu ikonoaren bertsio erabilgarria. Oihalez kanpoko objektuak kendu egingo dira eta SVG optimizatu egingo da tamaina txikiagoa izateko"

#: data/resources/ui/export.ui:50
msgid "Regular"
msgstr "Arrunta"

#: data/resources/ui/export.ui:64
msgid "Save Regular…"
msgstr "Gorde erregularra…"

#: data/resources/ui/export.ui:98
msgid "Nightly"
msgstr "Gauekoa"

#: data/resources/ui/export.ui:112
msgid "Save Nightly…"
msgstr "Gorde gauekoa…"

#: data/resources/ui/export.ui:148
msgid "Symbolic"
msgstr "Sinbolikoa"

#: data/resources/ui/export.ui:161
msgid "Save Symbolic…"
msgstr "Gorde sinbolikoa…"

#: data/resources/ui/help-overlay.ui:11
msgctxt "shortcut window"
msgid "Application"
msgstr "Aplikazioa"

#: data/resources/ui/help-overlay.ui:15
msgctxt "shortcut window"
msgid "New Window"
msgstr "_Leiho berria"

#: data/resources/ui/help-overlay.ui:21
msgctxt "shortcut window"
msgid "Open an Icon"
msgstr "Ireki ikono bat"

#: data/resources/ui/help-overlay.ui:27
msgctxt "shortcut window"
msgid "Quit the Application"
msgstr "Irten aplikaziotik"

#: data/resources/ui/help-overlay.ui:34
msgctxt "shortcut window"
msgid "View"
msgstr "Ikusi"

#: data/resources/ui/help-overlay.ui:38
msgctxt "shortcut window"
msgid "Reload the Icon"
msgstr "Birkargatu ikonoa"

#: data/resources/ui/help-overlay.ui:44
msgctxt "shortcut window"
msgid "Export the Icon"
msgstr "Esportatu ikonoa"

#: data/resources/ui/help-overlay.ui:50
msgctxt "shortcut window"
msgid "Shuffle Icons"
msgstr "Nahastu ikonoak"

#: data/resources/ui/help-overlay.ui:56
msgctxt "shortcut window"
msgid "Take Screenshot"
msgstr "Egin pantaila-argazkia"

#: data/resources/ui/help-overlay.ui:62
msgctxt "shortcut window"
msgid "Copy a Screenshot to Clipboard"
msgstr "Kopiatu pantaila-argazkia arbelean"

#: data/resources/ui/new_project.ui:7
msgid "New App Icon"
msgstr "Aplikazio-ikono berria"

#: data/resources/ui/new_project.ui:18
msgid "_Cancel"
msgstr "_Utzi"

#: data/resources/ui/new_project.ui:23
msgid "_Create"
msgstr "_Sortu"

#: data/resources/ui/new_project.ui:48
msgid "App Name"
msgstr "Aplikazioaren izena"

#: data/resources/ui/new_project.ui:58
msgid "Icon Location"
msgstr "Ikonoaren okalekua"

#: data/resources/ui/new_project.ui:78
msgid "The reverse domain notation name, e.g. org.inkscape.Inkscape"
msgstr "Domeinuen alderantzizko izenaren forman, adib. org.inkscape.Inkscape"

#: data/resources/ui/new_project.ui:92
msgid "The icon SVG file will be stored in this directory"
msgstr "Ikonoaren SVG fitxategia direktorio honetan gordeko da"

#: data/resources/ui/new_project.ui:108
msgid "~/Projects"
msgstr "~/Proiektuak"

#: data/resources/ui/new_project.ui:114
msgid "_Browse…"
msgstr "_Arakatu…"

#: data/resources/ui/window.ui:17
msgid "_Open"
msgstr "_Ireki"

#: data/resources/ui/window.ui:19
msgid "Open an Icon"
msgstr "Ireki ikono bat"

#: data/resources/ui/window.ui:25
msgid "Main Menu"
msgstr "Menu nagusia"

#: data/resources/ui/window.ui:32
msgid "_Export"
msgstr "_Esportatu"

#: data/resources/ui/window.ui:50
msgid "Make a new App Icon"
msgstr "Sortu aplikazio-ikono berria"

#: data/resources/ui/window.ui:176
msgid "_New App Icon"
msgstr "A_plikazio-ikono berria"

#: data/resources/ui/window.ui:197
msgid "_New Window"
msgstr "_Leiho berria"

#: data/resources/ui/window.ui:203
msgid "_Reload"
msgstr "Bir_kargatu"

#: data/resources/ui/window.ui:207
msgid "_Copy Screenshot"
msgstr "_Kopiatu pantaila-argazkia"

#: data/resources/ui/window.ui:211
msgid "_Save Screenshot"
msgstr "_Gorde pantaila-argazkia"

#: data/resources/ui/window.ui:215
msgid "_Shuffle Example Icons"
msgstr "_Nahastu adibideko ikonoak"

#: data/resources/ui/window.ui:221
msgid "_Keyboard Shortcuts"
msgstr "Las_ter-teklak"

#: data/resources/ui/window.ui:225
msgid "_About App Icon Preview"
msgstr "Aplikazio-ikonoen Aurrebista aplikazioari _buruz"

#: src/application.rs:159
msgid "translator-credits"
msgstr "translator-credits"

#: src/project.rs:161
msgid "SVG"
msgstr "SVG"

#: src/project.rs:168
msgid "Export"
msgstr "Esportatu"

#: src/project.rs:169 src/widgets/project_previewer.rs:217
msgid "_Save"
msgstr "_Gorde"

#: src/widgets/new_project.rs:58
msgid "Select Icon Location"
msgstr "Hautatu kokalekua ikonoarentzako"

#: src/widgets/new_project.rs:61
msgid "Select"
msgstr "Hautatu"

#: src/widgets/new_project.rs:61
msgid "Cancel"
msgstr "Utzi"

#: src/widgets/project_previewer.rs:185
msgid "Screenshot copied to clipboard"
msgstr "Pantaila-argazkia arbelean kopiatu da"

#: src/widgets/project_previewer.rs:209
msgid "PNG"
msgstr "PNG"

#: src/widgets/project_previewer.rs:215
msgid "Save Screenshot"
msgstr "Gorde pantaila-argazkia"

#: src/widgets/project_previewer.rs:218
msgid "Preview"
msgstr "Aurrebista"

#: src/widgets/window.rs:198
msgid "SVG images"
msgstr "SVG irudiak"

#: src/widgets/window.rs:202
msgid "Open File"
msgstr "Ireki fitxategia"

#~ msgid "Open an icon"
#~ msgstr "Ireki ikono bat"

#~ msgid "Welcome screen"
#~ msgstr "Ongi etorriko pantaila"

#~ msgid "Zander Brown"
#~ msgstr "Zander Brown"

#~ msgid "Preview icons"
#~ msgstr "Ikonoen aurrebista"

#~ msgid "Copyright © 2018-19 Zander Brown"
#~ msgstr "Copyright © 2018-19 Zander Brown"

#~ msgid "Repository"
#~ msgstr "Biltegia"

#~ msgctxt "shortcut window"
#~ msgid "Open the recent list"
#~ msgstr "Ireki azken erabilitakoen zerrenda"

#~ msgctxt "shortcut window"
#~ msgid "Open the menu"
#~ msgstr "Ireki menua"

#~ msgctxt "shortcut window"
#~ msgid "Take screenshot"
#~ msgstr "Egin pantaila-argazkia"

#~ msgctxt "shortcut window"
#~ msgid "Toggle fullscreen"
#~ msgstr "Txandakatu pantaila osoa"

#~ msgid "no longer supported"
#~ msgstr "jadanik ez da onartzen"

#~ msgid ""
#~ "Palette is all grown up!\n"
#~ "It’s now available separately as org.gnome.zbrown.Palette"
#~ msgstr ""
#~ "Paleta hazi egin da!\n"
#~ "Orain aparte eskura daiteke, izen honekin: org.gnome.zbrown.Palette"

#~ msgid "Close"
#~ msgstr "Itxi"

#~ msgid "Save…"
#~ msgstr "Gorde…"

#~ msgid "JPEG"
#~ msgstr "JPEG"

#~ msgid "Failed to save screenshot"
#~ msgstr "Huts egin du pantaila-argazkia egiteak"

#~ msgid "Button"
#~ msgstr "Botoia"

#~ msgid "Suggested"
#~ msgstr "Iradokia"

#~ msgid "Destructive"
#~ msgstr "Suntsigarria"

#~ msgid "Rename"
#~ msgstr "Aldatu izena"

#~ msgid ""
#~ "The icon is not recoloring because the file name needs to end in “-"
#~ "symbolic”"
#~ msgstr ""
#~ "Ikonoaren koloreak ez dira aldatzen fitxategi-izenak “-symbolic” duelako "
#~ "amaieran"

#~ msgid "New Symbolic Icon"
#~ msgstr "Ikono sinboliko erria"

#~ msgid "An application’s identity"
#~ msgstr "Aplikazio baten nortasuna"

#~ msgid "For actions, status and other things needed in apps"
#~ msgstr ""
#~ "Aplikazioetan behar diren ekintzetarako, egoeretarako eta beste zenbait "
#~ "gauzetarako"

#~ msgid "Recent"
#~ msgstr "Duela gutxi erabilita"

#~ msgid "This file is defective"
#~ msgstr "Fitxategi hau akastuna da"

#~ msgid ""
#~ "Please start from a template to ensure that your file will work as a "
#~ "GNOME icon"
#~ msgstr ""
#~ "Hasi txantiloi batetik zure fitxategiak GNOME ikono gisa funtzionatuko "
#~ "duela ziurtatzeko"

#~ msgid "Icons"
#~ msgstr "Ikonoak"

#~ msgid "Save"
#~ msgstr "Gorde"

#~ msgid "Icon"
#~ msgstr "Ikonoa"

#~ msgid "Failed to save exported file"
#~ msgstr "Huts egin du esportatuko fitxategia gordetzeak"

#~ msgid "Save Icon"
#~ msgstr "Gorde ikonoa"

#~ msgid "label"
#~ msgstr "etiketa"

#~ msgid "Icon Name"
#~ msgstr "Ikono-izena"

#~ msgid "All lowercase with dashes between words, e.g. list-add"
#~ msgstr ""
#~ "Dena minuskuletan, hitzen arteko marratxoekin, adib. zerrenda-gehitu"

#~ msgid "Expecting at least one “-”"
#~ msgstr "Gutxienez “-” bat espero zen"

#~ msgid "Expecting at least one “.”"
#~ msgstr "Gutxienez “.” bat espero zen"
